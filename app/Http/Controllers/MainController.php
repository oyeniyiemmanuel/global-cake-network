<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
    	return view('plain.homepage');
    }

    public function whoWeAre()
    {
    	return view('plain.who_we_are');
    }

    public function growWithUs()
    {
    	return view('plain.grow_with_us');
    }
}
