@extends('layout.master')

@section('title', 'Global Cake Network')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<!-- HOME -->
		<section id="home" class="padbot0">
				
			<!-- TOP SLIDER -->
			<div class="flexslider top_slider">
				<ul class="slides">
					<li class="slide1">
						<div class="flex_caption1">
							<p class="title2 captionDelay6 FromLeft">A Network of</p>
							<p class="title1 captionDelay4 FromLeft"> Vetted Bakers</p>
							<p class="title4 captionDelay7 FromLeft">Let us handle the techie part of your online presence, all you have to do is bake</p>
						</div>
						@if(1>2)
						<a class="slide_btn FromRight" href="javascript:void(0);" >Read More</a>
						@endif
					<li class="slide2">
						<div class="flex_caption1">
							<p class="title1 captionDelay2 FromTop">Simplified</p>
							<p class="title2 captionDelay4 FromTop">Cake</p>
							<p class="title3 captionDelay6 FromTop">Gifting</p>
							<p class="title4 captionDelay7 FromBottom">Want to gift to family and friends living in continents or cities apart? worry less as we connect you to that awesome baker</p>
						</div>
						@if(1>2)
						<a class="slide_btn FromRight" href="javascript:void(0);" >Read More</a>
						@endif
					</li>
					<li class="slide3">
						<div class="flex_caption1">
							<p class="title1 captionDelay1 FromBottom">Your Own</p>
							<p class="title2 captionDelay2 FromBottom"> Business</p>
							<p class="title3 captionDelay3 FromBottom"> Specialist</p>
							<p class="title4 captionDelay5 FromBottom"> As a baker, we assign you a personal business specialist to help you create and maintain that perfect online store you want with your own website domain. 
							</p>
						</div>
						<a class="slide_btn FromRight" href="javascript:void(0);" >Read More</a>
						
						<!-- VIDEO BACKGROUND -->
						<a id="P2" class="player" data-property="{videoURL:'tDvBwPzJ7dY',containment:'.top_slider .slide3',autoPlay:true, mute:true, startAt:0, opacity:1}" ></a>
						<!-- //VIDEO BACKGROUND -->
					</li>
				</ul>
			</div>
			<div id="carousel">
				<ul class="slides">
					<li><img src="images/slider/slide1_bg.jpg" alt="" /></li>
					<li><img src="images/slider/slide2_bg.jpg" alt="" /></li>
					<li><img src="images/slider/slide3_bg.jpg" alt="" /></li>
				</ul>
			</div><!-- //TOP SLIDER -->
		</section><!-- //HOME -->
		
		
		<!-- ABOUT -->
		<section id="about">
			
			<!-- SERVICES -->
			<div class="services_block padbot40" data-appear-top-offset="-200" data-animated="fadeInUp">
				
				<!-- CONTAINER -->
				<div class="container">
				
					<!-- ROW -->
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-ss-12 margbot30">
							<a class="services_item" href="javascript:void(0);" >
								<p>Easy Accessibility</p>
								<span>There's a customer out there right now trying to gift a cake to someone in your vicinity. why not get that gig?</span>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-ss-12 margbot30">
							<a class="services_item" href="javascript:void(0);" >
								<p>&dollar; &nbsp;&nbsp;&nbsp; &cent; &nbsp;&nbsp;&nbsp; &pound;</p>
								<span>Getting paid in Dollars or any foreign currency has never been easier</span>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-ss-12 margbot30">
							<a class="services_item" href="javascript:void(0);" >
								<p>Updates & Support</p>
								<span>Here's a very pertinent part of your business online and you need not worry about it as we handle it for you</span>
							</a>
						</div>
					</div><!-- //ROW -->
				</div><!-- //CONTAINER -->
			</div><!-- //SERVICES -->
			
			@if(1>2)
			<!-- CLEAN CODE -->
			<div class="cleancode_block">
				
				<!-- CONTAINER -->
				<div class="container" data-appear-top-offset="-200" data-animated="fadeInUp">
					
					<!-- CASTOM TAB -->
					<div id="myTabContent" class="tab-content">
						<div class="tab-pane fade in active clearfix" id="tab1">
							<p class="title"><b>Clean</b> Code</p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
						<div class="tab-pane fade clearfix" id="tab2">
							<p class="title"><b>Technical</b> Support</p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
						<div class="tab-pane fade clearfix" id="tab3">
							<p class="title"><b>Responsive</b></p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
						<div class="tab-pane fade clearfix" id="tab4">
							<p class="title"><b>Documentation</b></p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
						<div class="tab-pane fade clearfix" id="tab5">
							<p class="title"><b>Quality</b></p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
						<div class="tab-pane fade clearfix" id="tab6">
							<p class="title"><b>Support</b></p>
							<span>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</span>
						</div>
					</div>
					<ul id="myTab" class="nav nav-tabs">
						<li class="active"><a class="i1" href="#tab1" data-toggle="tab" ><i></i><span>Clean Code</span></a></li>
						<li><a class="i2" href="#tab2" data-toggle="tab" ><i></i><span>Support</span></a></li>
						<li><a class="i3" href="#tab3" data-toggle="tab" ><i></i><span>Responsive</span></a></li>
						<li><a class="i4" href="#tab4" data-toggle="tab" ><i></i><span>Documentation</span></a></li>
						<li><a class="i5" href="#tab5" data-toggle="tab" ><i></i><span>Quality</span></a></li>
						<li><a class="i6" href="#tab6" data-toggle="tab" ><i></i><span>Support</span></a></li>
					</ul><!-- CASTOM TAB -->
				</div><!-- //CONTAINER -->
			</div><!-- //CLEAN CODE -->
			
			<!-- MULTI PURPOSE -->
			<div class="purpose_block">
				
				<!-- CONTAINER -->
				<div class="container">
					
					<!-- ROW -->
					<div class="row">
					
						<div class="col-lg-7 col-md-7 col-sm-7" data-appear-top-offset="-200" data-animated="fadeInLeft">
							<h2><b>Multi-purpose</b> WordPress Theme</h2>
							<p>We tried to make very high-quality product and so our code is very neat and clean. Whatever anyone could improve and modify the template to your liking.</p>
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
							<a class="btn btn-active" href="javascript:void(0);" ><span data-hover="Yes I want it">Byu This theme</span></a>
							<a class="btn" href="javascript:void(0);" >View more templates</a>
						</div>
						
						<div class="col-lg-5 col-md-5 col-sm-5 ipad_img_in" data-appear-top-offset="-200" data-animated="fadeInRight">
							<img class="ipad_img1" src="images/img1.png" alt="" />
						</div>
					</div><!-- //ROW -->
				</div><!-- //CONTAINER -->
			</div><!-- //MULTI PURPOSE -->
		</section><!-- //ABOUT -->

		@endif
		
		
		<!-- PROJECTS -->
		<section id="projects" class="padbot20">
		
			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Featured</b> Works</h2>
			</div><!-- //CONTAINER -->
			
				
			<div class="projects-wrapper">
				<!-- PROJECTS SLIDER -->
				<div class="owl-demo owl-carousel projects_slider">
					
					<!-- work1 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/1.jpg" alt="" />
								<a class="zoom" href="images/works/1.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >ANL Cakes</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work1 -->
					
					<!-- work2 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/2.jpg" alt="" />
								<a class="zoom" href="images/works/2.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >Cathy Juliee</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work2 -->
					
					<!-- work3 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/3.jpg" alt="" />
								<a class="zoom" href="images/works/3.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >Sade Cakes</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work3 -->
					
					<!-- work4 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/4.jpg" alt="" />
								<a class="zoom" href="images/works/4.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >cake something</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work4 -->
					
					<!-- work5 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/5.jpg" alt="" />
								<a class="zoom" href="images/works/5.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >Good Cakes</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work5 -->
					
					<!-- work6 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/6.jpg" alt="" />
								<a class="zoom" href="images/works/6.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >CakeLand</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work6 -->
					
					<!-- work7 -->
					<div class="item">
						<div class="work_item">
							<div class="work_img">
								<img src="images/works/7.jpg" alt="" />
								<a class="zoom" href="images/works/7.jpg" rel="prettyPhoto[portfolio1]" ></a>
							</div>
							<div class="work_description">
								<div class="work_descr_cont">
									<a href="portfolio-post.html" >Cakecity</a>
									<span>Lagos, Nigeria</span>
								</div>
							</div>
						</div>
					</div><!-- //work7 -->
				</div><!-- //PROJECTS SLIDER -->
			</div>
			
			
			<!-- OUR CLIENTS -->
			<div class="our_clients">
			
				<!-- CONTAINER -->
				<div class="container">
					<h2>Partners</h2>
					<!-- ROW -->
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/1.jpg" alt="" />
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/2.jpg" alt="" />
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/3.jpg" alt="" />
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/4.jpg" alt="" />
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/5.jpg" alt="" />
						</div>
						<div class="col-lg-2 col-md-2 col-sm-2 client_img">
							<img src="images/clients/6.jpg" alt="" />
						</div>
					</div><!-- //ROW -->
				</div><!-- CONTAINER -->
			</div><!-- //OUR CLIENTS -->
		</section><!-- //PROJECTS -->
		
		@if(1 > 2)
		<!-- TEAM -->
		<section id="team">
		
			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Our</b> Team</h2>
				
				<!-- ROW -->
				<div class="row">
						
					<!-- TEAM SLIDER -->
					<div class="owl-demo owl-carousel team_slider">
				
						<!-- crewman1 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/1.jpg" alt="Olaniyi Oshinowo" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Olaniyi Oshinowo</p>
										<span>Director</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
									<a target="_blank" href="https://www.instagram.com/olan_osh" ><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div><!-- crewman1 -->
						
						<!-- crewman2 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/2.jpg" alt="Oyeniyi Emmanuel" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Oyeniyi Emmanuel</p>
										<span>CTO</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="https://twitter.com/gbenga_nuel" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="https://www.instagram.com/gbenganuel" ><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</div><!-- crewman1 -->
						
						<!-- crewman3 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/3.jpg" alt="Kemi" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Kemi</p>
										<span>Hr Manager</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman3 -->
						
						<!-- crewman4 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/4.jpg" alt="Sunny" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Sunny</p>
										<span>Buisness Specialist</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman4 -->
						
						<!-- crewman5 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/5.jpg" alt="Nonso" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Nonso</p>
										<span>Accountant</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman5 -->
						
						<!-- crewman6 -->
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/6.jpg" alt="Tade" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Tade</p>
										<span>Graphics Designer</span>
									</div>
								</div>
								<div class="crewman_social">
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a target="_blank" href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman6 -->
						
						<!-- crewman7 
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/7.jpg" alt="" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Joe Mades</p>
										<span>Developer</span>
									</div>
								</div>
								<div class="crewman_social">
									<a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman7 -->
						
						<!-- crewman8 
						<div class="item">
							<div class="crewman_item">
								<div class="crewman">
									<img src="images/team/8.jpg" alt="" />
								</div>
								<div class="crewman_descr center">
									<div class="crewman_descr_cont">
										<p>Julia Anderson</p>
										<span>Developer</span>
									</div>
								</div>
								<div class="crewman_social">
									<a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
									<a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
									<a href="javascript:void(0);" ><i class="fa fa-facebook-square"></i></a>
								</div>
							</div>
						</div><!-- crewman8 -->
					</div><!-- TEAM SLIDER -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
		</section><!-- //TEAM -->
		@endif
		
		
		<!-- NEWS -->
		<section id="news">
		
			<!-- CONTAINER -->
			<div class="container">
				<h2><b>Clients</b> say about us</h2>
				
				<!-- TESTIMONIALS -->
				<div class="testimonials">
						
					<!-- TESTIMONIALS SLIDER -->
					<div class="owl-demo owl-carousel testim_slider">
						
						<!-- TESTIMONIAL1 -->
						<div class="item">
							<div class="testim_content">
								“To harness the massive African youths work force, by empowering them on earning local and dollar currency via virtually home offices.”
							</div>
							<div class="testim_author">—  Kathy Juliee, <b>German based baker</b></div>
						</div><!-- TESTIMONIAL1 -->
						
						<!-- TESTIMONIAL2 -->
						<div class="item">
							<div class="testim_content">“educate and position  vetted bakers to take advantage of multiple income streams never before actualizied.”</div>
							<div class="testim_author">—  Sade Folusho, <b>Lagos State Commissioner of works</b></div>
						</div><!-- TESTIMONIAL2 -->
						
						<!-- TESTIMONIAL3 -->
						<div class="item">
							<div class="testim_content">“To connect customer to an untapped gifting experience, by simplified and innovative technology.”</div>
							<div class="testim_author">—  TY Danjuma, <b>Reputable politician</b></div>
						</div><!-- TESTIMONIAL3 -->
					</div><!-- TESTIMONIALS SLIDER -->
				</div><!-- //TESTIMONIALS -->
				
				
			</div><!-- //CONTAINER -->
		</section><!-- //NEWS -->
	</div><!-- //PAGE -->

@endsection

@section('footer')
	@parent
@endsection