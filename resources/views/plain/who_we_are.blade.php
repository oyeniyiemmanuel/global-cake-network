@extends('layout.master')

@section('title', 'Global Cake Network')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<!-- PORTFOLIO -->
		<section id="portfolio">
			
			<!-- CONTAINER -->
			<div class="container">
				
				<!-- ROW -->
				<div class="row">
					
					<!-- SIDEBAR -->
					<div class="sidebar col-lg-6 col-md-6 pull-right padbot50">
						<!-- TEXT WIDGET -->
						<div class="sidepanel widget_text">
							<div class="single_portfolio_post_title">About Us</div>
							<p>
							Our platform reconnects people living in cities and continents, 
							with a simple gift cake that reminds one that "you might be out of sight 
							but not out of mind" and ultimately letting the gifter feel as part of the
							celebration.
							</p> <br>
							<p>
							By harnesing innovative technology, we are able to simplify how family, friends
							& businesses gift cakes and various items worldwide, powered by a global network of
							locally vetted affiliate vendors.
							</p>
						</div><!-- //TEXT WIDGET -->
						
						<hr>
						
						<div class="col-lg-6 col-md-6">
							<!-- INFO WIDGET -->
							<div class="sidepanel widget_info">
								<ul class="work_info">
									<h5><b>Atlanta Office</b></h5>
									<li><b>3130, Sweet Basil Lane, Loganvile, GA. 30052</b></li>
								</ul>
							</div><!-- //INFO WIDGET -->
						</div>

						<div class="col-lg-6 col-md-6">
							<!-- INFO WIDGET -->
							<div class="sidepanel widget_info">
								<ul class="work_info">
									<h5><b>Lagos Office</b></h5>
									<li><b>2nd Floor, The Garnet Building, Lekki Express Road, Lekki, Lagos</b></li>
								</ul>
							</div><!-- //INFO WIDGET -->
						</div>

						<ul class="shared">
							<li><a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a></li>
							<li><a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
						
					</div><!-- //SIDEBAR -->
					
					
					<!-- PORTFOLIO BLOCK -->
					<div class="portfolio_block col-lg-6 col-md-6 pull-left padbot50">
						
						<!-- SINGLE PORTFOLIO POST -->
						<div class="single_portfolio_post clearfix" data-animated="fadeInUp">
							
							<!-- PORTFOLIO SLIDER -->
							<div class="flexslider portfolio_single_slider">
								<ul class="slides">
									<li><img src="images/portfolio/1.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_2.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_3.jpg" alt="" /></li>
									<li><img src="images/portfolio/1_4.jpg" alt="" /></li>
								</ul>
							</div><!-- //PORTFOLIO SLIDER -->
						</div><!-- //SINGLE PORTFOLIO POST -->
					</div><!-- //PORTFOLIO BLOCK -->
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
			
			
			<!-- CONTAINER -->
			<div class="container">
				
			</div><!-- //CONTAINER -->
			
		</section><!-- //PORTFOLIO -->

		<!-- BREADCRUMBS -->
		<section class="breadcrumbs_block clearfix parallax">
			<div class="container center">
				<h2>Our Vision</h2>
				<p>To become an innovative household global gifting platform</p>
				<br>
				<br>
				<br>
				<hr style="width: 30%;">
				<br>
				<br>
				<h2>Our Mission</h2>

				<div class="col-lg-2 col-lg-2"></div>
				<div class="col-lg-8 col-lg-8">
					<ul>
						<li>
							<i class="fa fa-dot-circle-o"></i> &nbsp;&nbsp; To connect customers to an untapped gifting experience,
							via simplified and innovative technology
						</li>
						<li>
							<i class="fa fa-dot-circle-o"></i> &nbsp;&nbsp; To educate and position vetted bakers to take advantage
							of multiple income streams that were never before actualized
						</li>
						<li>
							<i class="fa fa-dot-circle-o"></i> &nbsp;&nbsp; To harnes the massive african youths work force, by 
							empowering them on earning local and foreign currencies from their virtual home offices
						</li>
					</ul>
				</div>
				<div class="col-lg-2 col-lg-2"></div>
				
			</div>
		</section><!-- //BREADCRUMBS -->

	</div><!-- //PAGE -->


@endsection

@section('footer')
	@parent
@endsection