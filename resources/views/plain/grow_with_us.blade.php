@extends('layout.master')

@section('title', 'Global Cake Network')

@section('navigation_bar')
	@parent
@endsection

@section('content')

	<!-- BREADCRUMBS -->
	<section class="breadcrumbs_block clearfix parallax">
		<div class="container">
			<div class="col-lg-4 col-md-4">
				
			</div>
			<div class="col-lg-4 col-md-4">
				
			</div>
			<div class="col-lg-4 col-md-4">
				<h2><b>Careers </b> at GCN</h2>
				<p>
					At GCN, we are all about self empowerment and believing in the division of labor to benefit us as a team
				</p>
				<p>
					Our strength is grounded in our convictions  for <b>"UNITY & FAIRNESS"</b> as we consistently explore avenue that will passionately engage each and every team member.
				</p>
			</div>
		</div>
	</section><!-- //BREADCRUMBS -->

	<section id="news">
		<div class="container">
			<!-- RECENT POSTS -->
			<div class="row recent_posts">
				<div class="col-lg-4 col-md-4 col-sm-4 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="images/blog/1.jpg" alt="" />
							<a class="link" href="blog-post.html" ></a>
						</div>
						<div class="post_item_content">
							<a class="title" href="blog-post.html" >Students</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="images/blog/2.jpg" alt="" />
							<a class="link" href="blog-post.html"></a>
						</div>
						<div class="post_item_content">
							<a class="title" href="blog-post.html" >Graduates</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 padbot30 post_item_block">
					<div class="post_item">
						<div class="post_item_img">
							<img src="images/blog/3.jpg" alt="" />
							<a class="link" href="blog-post.html"></a>
						</div>
						<div class="post_item_content">
							<a class="title" href="blog-post.html" >Experienced professinals</a>
						</div>
					</div>
				</div>
			</div><!-- RECENT POSTS -->
		</div>
	</section>

	</div>

@endsection

@section('footer')
	@parent
@endsection